#!/usr/bin/env python2.6
# -*- coding: utf-8 -*-
#date 12/08/2016
#@by Samuel Dantas
""" Script to send infos of machine and borr process to monitor
 Using python-dmidecode package to get all informations and system commands """
import os
import dmidecode
import psutil
import json
import time
import re 

from asterisk import manager
from datetime import datetime
from websocket import create_connection

FreyConfig = {
    'client_id' : 'innovare20180703211533',
    'client_name' : 'Innovare',
    'client_web_host' : '',
    'client_web_port' : '8181',
    'client_ssh_host' : '',
    'client_ssh_port' : '22',
    'client_address' : 'Rua Paulo de Faria, 182, Vila Gustavo, São Paulo - SP',
    'frey_host' : '186.251.140.20',
    'frey_port' : '3001',
    'frey_manager_host' : 'localhost',
    'frey_manager_port' : '8426',
    'frey_manager_user' : 'borr',
    'frey_manager_password' : 'm4n4g3r*8426'
}

manager = manager.Manager()
try:
    manager.connect(FreyConfig['frey_manager_host'], FreyConfig['frey_manager_port'])
    manager.login(FreyConfig['frey_manager_user'], FreyConfig['frey_manager_password'])
except:
    pass


def toGb(data):
    """
    Convert byte to gigaByte
    """
    aBytes = 1073741824
    return float(data) / float(aBytes)

def getMemory():
    """
    Get memory used/free and capacity 
    No parameters
    Return :
        dict
    """
    mem_total = psutil.virtual_memory().total
    mem_used = psutil.virtual_memory().used
    mem_free = psutil.virtual_memory().free
    mem_aval = psutil.virtual_memory().available
    mem_dict = {
        "mem_total" : "%.2f" % toGb(mem_total),
        "mem_used" : "%.2f" % toGb(mem_used),
        "mem_free" : "%.2f" % toGb(mem_free),
        "mem_aval" : "%.2f" % toGb(mem_aval)
    }
    return mem_dict

def getCpu():
    """
    Get cpu informaitions 
    No parameters
    Return :
        dict 
    """
    cpu_cores = psutil.cpu_count()
    cpu_load_average = os.popen("uptime | awk '{print $10\"  \"$11\"  \"$12}'").read()\
            .replace('\n','')
    for row in dmidecode.processor().values():
        cpu_info = row['data']['Version']
    cpu_dict = {
        "cpu_cores" : cpu_cores,
        "cpu_load_average" : cpu_load_average,
        "cpu_info" : cpu_info
    }
    return cpu_dict

def getDisk():
    """
    Get disks informations
    No parameters
    Return:
        list of dicts
    """
    partitions = []
    for row in psutil.disk_partitions():
        partition = {}
        partition['device'] = row.device
        partition['usage'] = dict(psutil.disk_usage(row.mountpoint)._asdict())
        partition['mountpoint'] = row.mountpoint
        partition['type'] = row.fstype
        partitions.append(partition)
    return partitions

def getUptime():
    """
    Get datetime of machine uptime 
    No parameters
    Return:
        dict
    """
    bootTimeStr = datetime.fromtimestamp(psutil.boot_time()).strftime("%Y-%m-%d %H:%M:%S")
    bootTime = datetime.strptime(bootTimeStr, "%Y-%m-%d %H:%M:%S")
    now = datetime.now()
    uptime = (now - bootTime).seconds
    uptime_dict = {
        "boot_time" : bootTimeStr,
        "uptime" : uptime
    }
    return uptime_dict

def getNetwork():
    """
    Get network parameters and boards
    No parameters
    Return:
        list of dict
    """
    interfaces = []
    for row in psutil.net_if_addrs():
        interface = {}
        interface['interface'] = row
        interface['address'] = psutil.net_if_addrs()[row][0].address
        interface['netmask'] = psutil.net_if_addrs()[row][0].netmask
        interfaces.append(interface)
    return interfaces

def getUsers():
    """
    Get users loged at that machine
    No parameters
    Return:
        list of dict
    """
    users = []
    for row in psutil.users():
        row = dict(row._asdict())
        users.append(row)
    return users
       

def getMachineInfo():
    """
    Get all informations of that machine (so, model, vendor)
    No parameters
    Return:
        dict
    """
    for row in dmidecode.system().values():
        if isinstance(row, dict) and row['dmi_type'] == 1:
            machine_name = row['data']['Product Name']
            machine_vendor = row['data']['Manufacturer']
    machine_dict = {
        "machine_name" : machine_name,
        "machine_vendor" : machine_vendor
    }
    return machine_dict

def getActiveProcess():
    """
    Get the borr process and asterisk daemon status
    No parameters
    Return:
        dict
    """
    processes = {}
    listProcesses = []
    system_process = ['asterisk', 'mysqld', 'fail2ban-server']
    #open pids of borrApi
    with open('/var/run/borrApi.pid') as file:
        apiPids = [int(pid) for pid in file.read().split(',') if pid != '']
    borr_pids = apiPids
    dict_names = {"uwsgi" : "borr-api"}
    for proc in psutil.process_iter():
        try:
            pinfo = proc.as_dict(attrs=['pid','name'])
        except psutil.NoSuchProcess:
            pass
        else:
            if pinfo['name'] in system_process or pinfo['pid'] in borr_pids:
                if pinfo['name'] in dict_names:
                    pinfo['name'] = pinfo['name'].replace(
                        pinfo['name'], dict_names[pinfo['name']])
                listProcesses.append(pinfo)
    proc_names = [a.values()[1] for a in listProcesses]
    processes = {
        'borr_api' : 'online' if 'borr-api' in proc_names else 'offline',
        'borr_front' : 'online',
        'asterisk' : 'online' if 'asterisk' in proc_names else 'offline',
        'mysqld' : 'online' if 'mysqld' in proc_names else 'offline',
        'fail2ban_server' : 'online' if 'fail2ban-server' in proc_names else 'offline',
        'list' : listProcesses
    }
    return processes

def getActiveCalls():
    """
    Get all active calls 
    No parameters
    Return:
        dict
    """
    try: 
        command = manager.command('core show channels')
        for i in command.data.split('\n'):
            if re.search(".* active call.*", i):
                calls = i.split(' ')[0]
    except:
        calls = ''
    dict_active_calls = {
        'active_calls' : calls if calls != '' else 0
    }
    return dict_active_calls

def getActiveChannels():
    """
    Get all channels that are active
    No parameters
    Return :
        dict
    """
    channels = os.popen("asterisk -rc 'core show channels'\
                        | grep 'active channels' | awk '{print $1}'")
    channels = channels.read().replace('\n','')            
    dict_active_channels = {
        'active_channels' : channels if channels != '' else 0
    }
    return dict_active_channels

def generalStatus():
    """
    Main function to run all that 
    No arguments
    Return :
        dict        
    """
    main_dict = {
        "client_id" : FreyConfig['client_id'],
        "client_name" : FreyConfig['client_name'],
        "client_address" : FreyConfig['client_address'],
        "client_web_host" : FreyConfig['client_web_host'],
        "client_web_port" : FreyConfig['client_web_port'],
        "client_ssh_host" : FreyConfig['client_ssh_host'],
        "client_ssh_port" : FreyConfig['client_ssh_port'],
        "memory" : getMemory(),
        "disks" : getDisk(),
        "processors" : getCpu(),
        "network" : getNetwork(),
        "machine_info" : getMachineInfo(),
        "uptime" : getUptime(),
        "users" : getUsers(),
        "calls" : getActiveCalls(),
        "processes" : getActiveProcess(),
        "expanded" : False

    }
    return main_dict

def main():
    while True:
        data = json.dumps(generalStatus())
        try:
            ws.send(data)
        except:
            try:
                ws = create_connection("ws://{0}:{1}/ws".format(FreyConfig['frey_host'],
                                                                FreyConfig['frey_port']))
            except:
                pass
        time.sleep(2)
    result =  ws.recv()
    ws.close()

print "Starting frey client process\n"
#with daemon.DaemonContext():
main()
