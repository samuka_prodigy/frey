#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import json
import shelve

import dataset 
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template
import tornado.httpserver

from datetime import datetime

clients_path = "/opt/borr/frey/clients" 
clients = []
static_path = os.path.join(os.path.dirname(__file__), 'static')


def insert_or_update(client_dict):
    """
    Function to insert or update a client
    args:   client_id (string)
    return : offline_clients (list)
    """
    offline_clients = []
    action_type = ""
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    
    client = shelve.open("{0}/{1}".format(clients_path, client_dict["client_id"]))
    client['last_update'] = str(current_time)
    client['data'] = client_dict
    client.close()

    client_files = [file for dir,dirs,file in os.walk(clients_path)][0]

    for file in client_files:
        client = shelve.open("{0}/{1}".format(clients_path, file))

        client_last_time = client['last_update']
        client_last_time = datetime.strptime(client_last_time, "%Y-%m-%d %H:%M:%S")
        current_time = datetime.strptime(str(current_time), "%Y-%m-%d %H:%M:%S")

        time_difference = (current_time - client_last_time).seconds / 60
        
        if time_difference >= 1:
            offline_clients.append(client['data']['client_id'])
        
        try:
            client.close()
        except:
            pass

    return offline_clients


class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("user")

class MainHandler(BaseHandler):
    def get(self):
        if not self.current_user:
            self.redirect("/login")
            return
        self.render("index.html")

class LoginHandler(BaseHandler):
    def get(self):
        self.write('<html><body><form action="/login" method="post">'
                   'Name: <input type="text" name="username">'
                   'Password: <input type="password" name="password">'
                   '<input type="submit" value="Login">'
                   '</form></body></html>')
    
    def post(self):
        username = self.get_argument('username')
        password = self.get_argument('password')
        if username == 'admin' and password == 'admin@321':
            self.set_secure_cookie("user", username)
            self.redirect("/")
            return
        else:
            self.write("Username and password dont match")

class LogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("user")
        self.redirect("/login")

class WSHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        print 'connection opened...'
        self.external_ip =  self.request.remote_ip
        clients.append(self)

    def on_message(self, message): 
        for client in clients: #I make a loop at the all clients and send message to it
            messageDict = json.loads(message)
            messageDict['remote_ip'] = self.external_ip
                        
            offline_clients_list = insert_or_update(messageDict)

            client.write_message(json.dumps(messageDict))

            if len(offline_clients_list) > 0:
                for file in offline_clients_list:
                    client_dict = shelve.open("{0}/{1}".format(clients_path, file))
                    message_dict = client_dict['data']
                    message_dict['offline'] = True
                    client.write_message(json.dumps(message_dict))
                    client_dict.close()


    def on_close(self):
        clients.remove(self)
        print 'connection closed...'

handlers = [
    (r'/login', LoginHandler),
    (r'/logout', LogoutHandler),
    (r'/ws', WSHandler),
    (r'/', MainHandler),
    (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': static_path})
]
settings = {
    'debug' : True,
    'login_url' : '/login',
    'cookie_secret' : 'bZJc2sWbQLKos6GkHn/VB9oXwQt8S0R0kRvJ5/xJ89E='
}

application = tornado.web.Application(handlers, **settings) 

if __name__ == "__main__":
  server = tornado.httpserver.HTTPServer(application)
  server.bind(3001, '0.0.0.0')
  server.start()
  print "Application is running at port 9090 $PAPAI"
  tornado.ioloop.IOLoop.current().start()

