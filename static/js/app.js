var app = angular.module('frey',[]).config(function ($interpolateProvider){
  $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});

app.controller('FreyController', function($scope){
$scope.events = [];
$scope.ws = new WebSocket('ws://' + location.host + '/ws');
$scope.ws.onopen = function(){
  $scope.ws.onmessage = function(data){
    var event = JSON.parse(data.data);
    var eventIndex = getEventIndex(event.client_id);
    if(eventIndex >= 0){
      event.expanded = $scope.events[eventIndex].expanded
      $scope.events[eventIndex] = event;
    } else {
      $scope.events.push(event);
    }
    $scope.$apply();
  }
}

/**
 * Function to return object index of event list
 */
function getEventIndex(event_id){
  return  $scope.events.indexOf($scope.events.filter(function(x){ return x.client_id == event_id})[0]);
}

});
